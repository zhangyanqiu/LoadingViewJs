/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import Animator from "@ohos.animator";
import {LVCircularCDController} from '../../common/component/LVCircularCD/LVCircularCDController.js'
import {LVCircularRingController} from '../../common/component/LVCircularRing/LVCircularRingController.js'
import {LVCircularController} from '../../common/component/LVCircular/LVCircularController.js'
import {LVFivePoiStarController} from '../../common/component/LVFivePoiStar/LVFivePoiStarController.js'
import {LVSmileController} from '../../common/component/LVSmile/LVSmileController.js'
import {LVGearsController} from '../../common/component/LVGears/LVGearsController.js'
import {LVGearsTwoController} from '../../common/component/LVGearsTwo/LVGearsTwoController.js'
import {LVWifiController} from '../../common/component/LVWifi/LVWifiController.js'
import {LVCircularJumpController} from '../../common/component/LVCircularJump/LVCircularJumpController.js'
import {LVCircularZoomController} from '../../common/component/LVCircularZoom/LVCircularZoomController.js'
import {LVPlayBallController} from '../../common/component/LVPlayBall/LVPlayBallController.js'
import {LVNewsController} from '../../common/component/LVNews/LVNewsController.js'
import {LVLineWithTextController} from '../../common/component/LVLineWithText/LVLineWithTextController.js'
import {LVEatBeansController} from '../../common/component/LVEatBeans/LVEatBeansController.js'
import {LVChromeLogoController} from '../../common/component/LVChromeLogo/LVChromeLogoController.js'
import {LVImgController} from '../../common/component/LVImg/LVImgController.js'
import {LVBlockController} from '../../common/component/LVBlock/LVBlockController.js'
import {LVFunnyBarController} from '../../common/component/LVFunnyBar/LVFunnyBarController.js'
import {LVGhostController} from '../../common/component/LVGhost/LVGhostController.js'
import {LVBatteryController} from '../../common/component/LVBattery/LVBatteryController.js'

export default {
    data: {
        animator: null,
        lvCircularCDController: null,
        lvCircularRingController: null,
        lvCircularController: null,
        lvFivePoiStarController: null,
        lvSmileController: null,
        lvGearsController: null,
        lvGearsTwoController: null,
        lvWifiController: null,
        lvCircularJumpController: null,
        lvCircularZoomController: null,
        lvPlayBallController: null,
        lvNewsController: null,
        lvLineWithTextController: null,
        lvEatBeansController: null,
        lvChromeLogoController: null,
        lvImgController: null,
        lvBlockController: null,
        lvFunnyBarController: null,
        lvGhostController: null,
        lvBatteryController: null,
    },
    onInit() {
        this.lvCircularCDController = new LVCircularCDController()
        .setViewColor("#00FF00")

        this.lvCircularRingController = new LVCircularRingController()
        .setViewColor("#64FFFFFF").setBarColor("#FFFF00")

        this.lvCircularController = new LVCircularController()
        .setViewColor("rgb(255,99,99)").setRoundColor("rgb(255,0,0)")

        this.lvFivePoiStarController = new LVFivePoiStarController()
        .setViewColor("#FFFFFF").setCircleColor("#FFFF00")

        this.lvSmileController = new LVSmileController()
        .setViewColor("rgb(144, 238, 146)")

        this.lvGearsController = new LVGearsController()
        .setViewColor("rgb(55, 155, 233)")

        this.lvGearsTwoController = new LVGearsTwoController()
        .setViewColor("rgb(155, 55, 233)")

        this.lvWifiController = new LVWifiController()
        .setViewColor("black")

        this.lvCircularJumpController = new LVCircularJumpController()
        .setViewColor("rgb(133, 66, 99)")

        this.lvCircularZoomController = new LVCircularZoomController()
        .setViewColor("rgb(255, 0, 122)")

        this.lvPlayBallController = new LVPlayBallController()
        .setViewColor("white").setBallColor("red")

        this.lvNewsController = new LVNewsController()
        .setViewColor("white")

        this.lvLineWithTextController = new LVLineWithTextController()
        .setViewColor("rgb(33, 66, 77)").setTextColor("rgb(233, 166, 177)")

        this.lvEatBeansController = new LVEatBeansController()
        .setViewColor("white").setEyesColor("blue")

        this.lvChromeLogoController = new LVChromeLogoController()

        this.lvImgController = new LVImgController()
        .setImgPath('/common/images/loading_white.png')

        this.lvBlockController = new LVBlockController()
        .setViewColor("rgb(245, 209, 22)")

        this.lvFunnyBarController = new LVFunnyBarController()
        .setViewColor("rgb(234, 167, 107)")

        this.lvGhostController = new LVGhostController()
        .setViewColor("white").setEyesColor("black")

        this.lvBatteryController = new LVBatteryController()
        .setViewColor("white").setCellColor("rgb(67, 213, 81)")

    },
    start1() {
        this.stopAll()
        this.$child('lvCircularCD').startAnim()
        this.$child('lvCircularRing').startAnim()
        this.$child('lvCircular').startAnim()
        this.$child('lvFivePoiStar').startAnim()
    },
    start2() {
        this.stopAll()
        this.$child('lvSmile').startAnim()
        this.$child('lvGears').startAnim()
        this.$child('lvGearsTwo').startAnim()
        this.$child('lvWifi').startAnim()
    },
    start3() {
        this.stopAll()
        this.$child('lvCircularJump').startAnim()
        this.$child('lvCircularZoom').startAnim()
        this.$child('lvPlayBall').startAnim()
        this.$child('lvNews').startAnim()
    },
    start4() {
        this.stopAll()
        this.startLVLineWithText()
        this.$child('lvEatBeans').startAnim()
    },
    start5() {
        this.stopAll()
        this.$child('lvChromeLogo').startAnim()
        this.$child('lvImg').startAnim()
        this.$child('lvBlock').startAnim()
        this.$child('lvFunnyBar').startAnim()
    },
    start6() {
        this.stopAll()
        this.$child('lvGhost').startAnim()
        this.$child('lvBattery').startAnim()
    },
    stopAll() {
        this.$child('lvCircularCD').stopAnim()
        this.$child('lvCircularRing').stopAnim()
        this.$child('lvCircular').stopAnim()
        this.$child('lvFivePoiStar').stopAnim()
        this.$child('lvSmile').stopAnim()
        this.$child('lvGears').stopAnim()
        this.$child('lvGearsTwo').stopAnim()
        this.$child('lvWifi').stopAnim()
        this.$child('lvCircularJump').stopAnim()
        this.$child('lvCircularZoom').stopAnim()
        this.$child('lvPlayBall').stopAnim()
        this.$child('lvNews').stopAnim()
        this.stopLVLineWithText()
        this.$child('lvEatBeans').stopAnim()
        this.$child('lvChromeLogo').stopAnim()
        this.$child('lvImg').stopAnim()
        this.$child('lvBlock').stopAnim()
        this.$child('lvFunnyBar').stopAnim()
        this.$child('lvGhost').stopAnim()
        this.$child('lvBattery').stopAnim()
    },
    startLVLineWithText() {
        if (this.animator == null) {
            var lVLineWithText = this.$child('lvLineWithText')
            var options = {
                duration: 5000,
                easing: 'linear',
                iterations: 1,
                begin: 1,
                end: 100
            };
            this.animator = Animator.createAnimator(options);
            this.animator.onframe = function (value) {
                lVLineWithText.setValue(value)
            };
        }
        this.animator.play();
    },
    stopLVLineWithText() {
        if (this.animator != null) {
            this.animator.pause()
            this.animator = null
        }
    }
}

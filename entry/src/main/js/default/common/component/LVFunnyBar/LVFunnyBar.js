/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVFunnyBarController} from '../../component/LVFunnyBar/LVFunnyBarController.js'

export default {
    props: {
        controller: {
            default: new LVFunnyBarController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        drawHeight: 0,
        heightPadding: 0,
        wspace: 0,
        hspace: 0,
        viewColor: "rgb(247,202,42)",
        viewColorLeft: "rgb(227,144,11)",
        viewColorRight: "rgb(188,91,26)",
    },
    onAttached() {
        this.viewColor = this.controller.getViewColor()
        this.viewColorLeft = this.controller.getViewColorLeft()
        this.viewColorRight = this.controller.getViewColorRight()
        this.controller.setAnimRepeatMode(true)
        this.controller.setIsBack(true)
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.drawHeight = parseInt(this.compWidth / Math.sqrt(3))
        this.heightPadding = (this.compWidth - this.drawHeight) / 2 > 0 ? (this.compWidth - this.drawHeight) / 2 : 0
        this.wspace = this.compWidth / 8
        this.hspace = this.drawHeight / 8
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';

        var leftLong
        // 左右都需要绘制3根
        for (var i = 0; i < 3; i++) {
            leftLong = this.controller.getAnimValue() * (1 + i / 4);
            if (leftLong > 1) {
                leftLong = 1;
            }
            var wlong = this.compWidth / 2 * leftLong - this.wspace / 2;
            var hlong = this.drawHeight / 2 * leftLong - this.hspace / 2;
            if (wlong < this.wspace / 8 / 8 / 2) {
                wlong = this.wspace / 8 / 8 / 2;
            }
            if (hlong < this.hspace / 8 / 8 / 2) {
                hlong = this.wspace / 8 / 8 / 2;
            }

            // 左边第一根上面
            ctx.fillStyle = this.viewColor;
            ctx.beginPath();
            ctx.moveTo((i + 0.5) * this.wspace, this.drawHeight / 2 + (i * this.hspace) + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace + wlong, this.drawHeight / 2 - this.hspace / 2 + (i * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo((i + 1.5) * this.wspace + wlong, this.drawHeight / 2 + (i * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (i * this.hspace) + this.heightPadding);
            ctx.closePath();
            ctx.fill()
            if (i == 0) {
                // 切换回默认显示模式，否则上面绘制的图像将不显示
                ctx.globalCompositeOperation = 'source-over';
            }

            // 左边第一根左面
            ctx.fillStyle = this.viewColorLeft;
            ctx.beginPath();
            ctx.moveTo((i + 0.5) * this.wspace, this.drawHeight / 2 + (i * this.hspace) + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (i * this.hspace) + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (i * this.hspace) + this.hspace + this.heightPadding);
            ctx.lineTo((i + 0.5) * this.wspace, this.drawHeight / 2 + (i * this.hspace) + this.hspace + this.heightPadding);
            ctx.closePath();
            ctx.fill()

            // 左边第一根右面
            ctx.fillStyle = this.viewColorRight;
            ctx.beginPath();
            ctx.moveTo((i + 1.5) * this.wspace + wlong, this.drawHeight / 2 + (i * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (i * this.hspace) + this.heightPadding);
            ctx.lineTo((i + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (i * this.hspace) + this.hspace + this.heightPadding);
            ctx.lineTo((i + 1.5) * this.wspace + wlong, this.drawHeight / 2 + (i * this.hspace) + this.hspace - hlong + this.heightPadding);
            ctx.closePath();
            ctx.fill()

            var rightPosition = i;
            // 右边第一根上面
            ctx.fillStyle = this.viewColor;
            ctx.beginPath();
            ctx.moveTo(this.compWidth - (rightPosition + 1.5) * this.wspace - wlong, this.drawHeight / 2 + (rightPosition * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace - wlong, this.drawHeight / 2 - this.hspace / 2 + (rightPosition * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 0.5) * this.wspace, this.drawHeight / 2 + (rightPosition * this.hspace) + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (rightPosition * this.hspace) + this.heightPadding);
            ctx.closePath();
            ctx.fill()

            // 右边第一根左面
            ctx.fillStyle = this.viewColorLeft;
            ctx.beginPath();
            ctx.moveTo(this.compWidth - (rightPosition + 1.5) * this.wspace - wlong, this.drawHeight / 2 + (rightPosition * this.hspace) - hlong + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (rightPosition * this.hspace) + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (rightPosition * this.hspace) + this.hspace + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1.5) * this.wspace - wlong, this.drawHeight / 2 + (rightPosition * this.hspace) + this.hspace - hlong + this.heightPadding);
            ctx.closePath();
            ctx.fill()

            // 右边第一根右面
            ctx.fillStyle = this.viewColorRight;
            ctx.beginPath();
            ctx.moveTo(this.compWidth - (rightPosition + 0.5) * this.wspace, this.drawHeight / 2 + (rightPosition * this.hspace) + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (rightPosition * this.hspace) + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 1) * this.wspace, this.drawHeight / 2 + this.hspace / 2 + (rightPosition * this.hspace) + this.hspace + this.heightPadding);
            ctx.lineTo(this.compWidth - (rightPosition + 0.5) * this.wspace, this.drawHeight / 2 + (rightPosition * this.hspace) + this.hspace + this.heightPadding);
            ctx.closePath();
            ctx.fill()
        }

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

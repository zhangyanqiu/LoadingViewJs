/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVEatBeansController} from '../../component/LVEatBeans/LVEatBeansController.js'

export default {
    props: {
        controller: {
            default: new LVEatBeansController()
        }
    },
    data: {
        visable1: "hidden",
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        padding: 5,
        eatErWidth: 60,
        eatErPositonX: 0,
        eatSpeed: 5,
        beansWidth: 10,
        mAngle: 34,
        eatErStrtAngle: 34,
        eatErEndAngle: 326,
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        this.eatErPositonX = (this.compWidth - 2 * this.padding - this.eatErWidth) * this.controller.getAnimValue();
        this.eatErStrtAngle = this.mAngle * (1 - (this.controller.getAnimValue() * this.eatSpeed - parseInt(this.controller.getAnimValue() * this.eatSpeed)));
        this.eatErEndAngle = 360 - this.eatErStrtAngle;
        var eatRightX = this.padding + this.eatErWidth + this.eatErPositonX;
        this.drawEatEr(ctx)
        this.drawBeans(ctx, eatRightX)

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable1 = "hidden"
            this.visable2 = "visible"
        } else {
            this.visable1 = "visible"
            this.visable2 = "hidden"
        }

    },
    drawEatEr(ctx) {
        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.fillStyle = this.controller.getViewColor();
        ctx.beginPath()
        ctx.arc(this.padding + this.eatErWidth / 2 + this.eatErPositonX, this.compHeight / 2,
            this.eatErWidth / 2,
            this.eatErStrtAngle / 360 * Math.PI * 2, (this.eatErEndAngle) / 360 * Math.PI * 2);
        ctx.lineTo(this.padding + this.eatErWidth / 2 + this.eatErPositonX, this.compHeight / 2)
        ctx.closePath()
        ctx.fill()
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        ctx.fillStyle = this.controller.getEyesColor();
        ctx.beginPath()
        ctx.arc(this.padding + this.eatErWidth / 2 + this.eatErPositonX, this.compHeight / 2 - this.eatErWidth / 4,
            this.beansWidth / 2,
            0, Math.PI * 2);
        ctx.fill()
    },
    drawBeans(ctx, eatRightX) {
        var beansCount = parseInt((this.compWidth - this.padding * 2 - this.eatErWidth) / this.beansWidth / 2);
        ctx.fillStyle = this.controller.getViewColor();
        for (var i = 0; i < beansCount; i++) {
            var x = beansCount * i + this.beansWidth / 2 + this.padding + this.eatErWidth;
            if (x > eatRightX) {
                ctx.beginPath()
                ctx.arc(x, this.compHeight / 2,
                    this.beansWidth / 2,
                    0, Math.PI * 2);
                ctx.fill()
            }
        }
    },
    drawOthers(ctx, radius) {
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2 + this.minWidth / this.signalSize,
            radius,
            -135 / 360 * Math.PI * 2, (-45) / 360 * Math.PI * 2);
        ctx.stroke();
    },
    startAnim() {
        this.controller.startViewAnim(3500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

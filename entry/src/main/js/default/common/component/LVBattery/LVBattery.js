/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBatteryController} from '../../component/LVBattery/LVBatteryController.js'
import {Rect} from '../../component/base/Rect.js'

export default {
    props: {
        controller: {
            default: new LVBatteryController()
        }
    },
    data: {
        visable1: "hidden",
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
        mBodyCorner: 3,
        mBodyValueCorner: 1,
        mBatterySpace: 3,
        mHeadwidth: 0,
        rectFBody: new Rect(),
        rectFBodyValue: new Rect(),
        mVlaue: 0,
        isNotAnim: false
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        if (this.controller.getPaddingVertical() != -1) {
            this.paddingVertical = this.controller.getPaddingVertical()
            this.paddingHorizontal = this.controller.getPaddingHorizontal()
        } else {
            this.paddingVertical = 6
            this.paddingHorizontal = (this.compWidth - (this.compHeight - 12 - this.mBatterySpace * 2) / 3) / 2
        }
        this.mHeadwidth = (this.compWidth - this.paddingHorizontal * 2) / 3
        this.rectFBody.top = this.paddingVertical + this.mHeadwidth / 2
        this.rectFBody.bottom = this.compHeight - this.paddingVertical
        this.rectFBody.left = this.paddingHorizontal
        this.rectFBody.right = this.compWidth - this.paddingHorizontal
        this.rectFBodyValue.top = 0
        this.rectFBodyValue.bottom = this.rectFBody.bottom - this.mBatterySpace
        this.rectFBodyValue.left = this.rectFBody.left + this.mBatterySpace
        this.rectFBodyValue.right = this.rectFBody.right - this.mBatterySpace
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }
        if (!this.isNotAnim) {
            this.mVlaue = this.controller.getAnimValue()
        }

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        this.drawHead(ctx);
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        this.drawBody(ctx);
        this.drawValue(ctx);
        this.drawText(ctx);

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable1 = "hidden"
            this.visable2 = "visible"
        } else {
            this.visable1 = "visible"
            this.visable2 = "hidden"
        }

        this.isNotAnim = false
    },
    drawHead(ctx) {
        ctx.fillStyle = this.controller.getViewColor()
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.paddingVertical + this.mHeadwidth / 2,
            this.mHeadwidth / 2,
            -320 / 360 * Math.PI, -40 / 360 * Math.PI);
        ctx.closePath();
        ctx.fill()
    },
    drawBody(ctx) {
        ctx.beginPath();
        ctx.strokeStyle = this.controller.getViewColor()
        ctx.arc(this.rectFBody.left + this.mBodyCorner, this.rectFBody.top + this.mBodyCorner, this.mBodyCorner, -Math.PI, -Math.PI / 2)
        ctx.lineTo(this.rectFBody.right - this.mBodyCorner, this.rectFBody.top)
        ctx.arc(this.rectFBody.right - this.mBodyCorner, this.rectFBody.top + this.mBodyCorner, this.mBodyCorner, -Math.PI / 2, 0)
        ctx.lineTo(this.rectFBody.right, this.rectFBody.bottom - this.mBodyCorner)
        ctx.arc(this.rectFBody.right - this.mBodyCorner, this.rectFBody.bottom - this.mBodyCorner, this.mBodyCorner, 0, Math.PI / 2)
        ctx.lineTo(this.rectFBody.left + this.mBodyCorner, this.rectFBody.bottom)
        ctx.arc(this.rectFBody.left + this.mBodyCorner, this.rectFBody.bottom - this.mBodyCorner, this.mBodyCorner, Math.PI / 2, Math.PI)
        ctx.lineTo(this.rectFBody.left, this.rectFBody.top + this.mBodyCorner)
        ctx.stroke();
    },
    drawValue(ctx) {
        this.rectFBodyValue.top = this.rectFBodyValue.bottom - (this.rectFBodyValue.bottom - this.rectFBody.top - this.mBatterySpace) * this.controller.getAnimValue()
        ctx.beginPath();
        ctx.fillStyle = this.controller.getCellColor()
        ctx.arc(this.rectFBodyValue.left + this.mBodyValueCorner, this.rectFBodyValue.top + this.mBodyValueCorner, this.mBodyValueCorner, -Math.PI, -Math.PI / 2)
        ctx.lineTo(this.rectFBodyValue.right - this.mBodyValueCorner, this.rectFBodyValue.top)
        ctx.arc(this.rectFBodyValue.right - this.mBodyValueCorner, this.rectFBodyValue.top + this.mBodyValueCorner, this.mBodyValueCorner, -Math.PI / 2, 0)
        ctx.lineTo(this.rectFBodyValue.right, this.rectFBodyValue.bottom - this.mBodyValueCorner)
        ctx.arc(this.rectFBodyValue.right - this.mBodyValueCorner, this.rectFBodyValue.bottom - this.mBodyValueCorner, this.mBodyValueCorner, 0, Math.PI / 2)
        ctx.lineTo(this.rectFBodyValue.left + this.mBodyValueCorner, this.rectFBodyValue.bottom)
        ctx.arc(this.rectFBodyValue.left + this.mBodyValueCorner, this.rectFBodyValue.bottom - this.mBodyValueCorner, this.mBodyValueCorner, Math.PI / 2, Math.PI)
        ctx.lineTo(this.rectFBodyValue.left, this.rectFBodyValue.top + this.mBodyValueCorner)
        ctx.fill();
    },
    drawText(ctx) {
        ctx.fillStyle = this.controller.getViewColor()
        ctx.textBaseline = 'middle';
        ctx.font = this.controller.getTextSize() + 'px sans-serif';
        var text = parseInt(this.controller.getAnimValue() * 100) + "%";
        var textWidth = ctx.measureText(text).width;
        ctx.fillText(text, this.rectFBody.getCenterX() - textWidth / 2, this.rectFBody.getCenterY());
    },
    startAnim() {
        this.controller.startViewAnim(5000, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
    setValue(value) {
        this.mVlaue = parseInt(value) / 100;
        this.isNotAnim = true
        this.onDraw()
    }
}

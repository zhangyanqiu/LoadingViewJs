/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBase} from '../base/LVBase.js'

export class LVBatteryController extends LVBase {
    animValue = 0;
    viewColor = 'white';
    cellColor = 'rgb(67, 213, 81)';
    textSize = 40;
    paddingVertical = -1;
    paddingHorizontal = -1;

    getAnimValue() {
        return this.animValue;
    }

    getViewColor() {
        return this.viewColor;
    }

    setViewColor(viewColor) {
        this.viewColor = viewColor;
        return this;
    }

    getPaddingVertical() {
        return this.paddingVertical;
    }

    getPaddingHorizontal() {
        return this.paddingHorizontal;
    }

    setPadding(paddingVertical, paddingHorizontal) {
        this.paddingVertical = paddingVertical;
        this.paddingHorizontal = paddingHorizontal;
        return this;
    }

    getCellColor() {
        return this.cellColor;
    }

    setCellColor(cellColor) {
        this.cellColor = cellColor;
        return this;
    }

    getTextSize() {
        return this.textSize;
    }

    setTextSize(textSize) {
        this.textSize = textSize;
        return this;
    }

    OnAnimationUpdate(value) {
        this.animValue = value
    }

    OnAnimationStop() {
        this.animValue = 0
    }
}
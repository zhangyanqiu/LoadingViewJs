/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVPlayBallController} from '../../component/LVPlayBall/LVPlayBallController.js'

export default {
    props: {
        controller: {
            default: new LVPlayBallController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 5,
        mRadius: 9,
        mStrokeWidth: 2,
        mRadiusBall: 12,
        quadToStart: 0,
        ballY: 0
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })

        var animValue = this.controller.getAnimValue()
        if (animValue == 0) {
            this.quadToStart = this.compHeight / 2;
            this.ballY = this.compHeight / 2;
        } else {
            if (animValue > 0.75) {
                this.quadToStart = this.compHeight / 2 - (1 - animValue) * this.compHeight / 3;
            } else {
                this.quadToStart = this.compHeight / 2 + (1 - animValue) * this.compHeight / 3;
            }
            if (animValue > 0.35) {
                this.ballY = this.compHeight / 2 - (this.compHeight / 2 * animValue);
            } else {
                this.ballY = this.compHeight / 2 + (this.compHeight / 6 * animValue);
            }
        }

        ctx.lineWidth = this.mStrokeWidth;
        ctx.strokeStyle = this.controller.getViewColor();
        ctx.fillStyle = this.controller.getBallColor();

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.beginPath();
        ctx.arc(this.padding + this.mRadius + this.mStrokeWidth, this.compHeight / 2, // 圆心
            this.mRadius, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.moveTo(this.padding + this.mRadius * 2 + this.mStrokeWidth, this.compHeight / 2);
        ctx.quadraticCurveTo(this.compWidth / 2, this.quadToStart, this.compWidth - this.padding - this.mRadius * 2 - this.mStrokeWidth, this.compHeight / 2);
        ctx.arc(this.compWidth - this.padding - this.mRadius - this.mStrokeWidth, this.compHeight / 2, // 圆心
            this.mRadius, // 半径
            -Math.PI, Math.PI); // 起始点和绘制弧度
        ctx.stroke();
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';

        ctx.beginPath();
        if (this.ballY - this.mRadiusBall > this.mRadiusBall) {
            ctx.arc(this.compWidth / 2, this.ballY - this.mRadiusBall, // 圆心
                this.mRadiusBall, // 半径
                0, Math.PI * 2); // 起始点和绘制弧度
        } else {
            ctx.arc(this.compWidth / 2, this.mRadiusBall, // 圆心
                this.mRadiusBall, // 半径
                0, Math.PI * 2); // 起始点和绘制弧度
        }
        ctx.fill();

    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

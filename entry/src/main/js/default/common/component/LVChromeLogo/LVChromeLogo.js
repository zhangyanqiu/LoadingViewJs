/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVChromeLogoController} from '../../component/LVChromeLogo/LVChromeLogoController.js'
import {CanvasRotateUtil} from '../../component/base/CanvasRotateUtil.js'
import {Point} from '../../component/base/Point.js'

export default {
    props: {
        controller: {
            default: new LVChromeLogoController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 3,
        yellow: "rgb(253, 197, 53)",
        green: "rgb(27, 147, 76)",
        red: "rgb(211, 57, 53)",
        canvasRotateUtil1: null,
        canvasRotateUtil2: null,
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        // 初始化画布旋转工具类
        if (this.canvasRotateUtil1 == null) {
            this.canvasRotateUtil1 = new CanvasRotateUtil(this.compWidth, this.compHeight)
            this.canvasRotateUtil2 = new CanvasRotateUtil(this.compWidth, this.compHeight)
        }
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        var rotate = 360 * this.controller.getAnimValue()
        // 通过工具类让画布绕中心点旋转
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.canvasRotateUtil2.rotate(ctx, rotate)
        } else {
            this.canvasRotateUtil1.rotate(ctx, rotate)
        }

        // 将圆分成三个扇形
        this.drawSector(ctx)
        // 画三个等边三角形组成的大三角形正好是内切三角形
        this.drawTriangle(ctx)
        // 画中心的圆覆盖
        this.drawCircle(ctx)

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    drawSector(ctx) {
        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.fillStyle = this.yellow;
        ctx.beginPath();
        ctx.arc(this.minWidth / 2, this.minWidth / 2,
            this.minWidth / 2 - this.padding,
            -30 / 360 * Math.PI * 2, 90 / 360 * Math.PI * 2);
        ctx.lineTo(this.minWidth / 2, this.minWidth / 2)
        ctx.fill()
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        ctx.fillStyle = this.green;
        ctx.beginPath();
        ctx.arc(this.minWidth / 2, this.minWidth / 2,
            this.minWidth / 2 - this.padding,
            90 / 360 * Math.PI * 2, 210 / 360 * Math.PI * 2);
        ctx.lineTo(this.minWidth / 2, this.minWidth / 2)
        ctx.fill()
        ctx.fillStyle = this.red;
        ctx.beginPath();
        ctx.arc(this.minWidth / 2, this.minWidth / 2,
            this.minWidth / 2 - this.padding,
            210 / 360 * Math.PI * 2, 330 / 360 * Math.PI * 2);
        ctx.lineTo(this.minWidth / 2, this.minWidth / 2)
        ctx.fill()
    },
    drawTriangle(ctx) {
        var point1 = this.getPoint((this.minWidth / 2 - this.padding) / 2, 90);
        var point2 = this.getPoint((this.minWidth / 2 - this.padding), 150);
        var point3 = this.getPoint((this.minWidth / 2 - this.padding) / 2, 210);
        var point4 = this.getPoint((this.minWidth / 2 - this.padding), 270);
        var point5 = this.getPoint((this.minWidth / 2 - this.padding) / 2, 330);
        var point6 = this.getPoint((this.minWidth / 2 - this.padding), 30);
        ctx.fillStyle = this.yellow;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - point1.x, this.minWidth / 2 - point1.y);
        ctx.lineTo(this.minWidth / 2 - point2.x, this.minWidth / 2 - point2.y);
        ctx.lineTo(this.minWidth / 2 - point3.x, this.minWidth / 2 - point3.y);
        ctx.closePath();
        ctx.fill()
        ctx.fillStyle = this.green;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - point3.x, this.minWidth / 2 - point3.y);
        ctx.lineTo(this.minWidth / 2 - point4.x, this.minWidth / 2 - point4.y);
        ctx.lineTo(this.minWidth / 2 - point5.x, this.minWidth / 2 - point5.y);
        ctx.closePath();
        ctx.fill()
        ctx.fillStyle = this.red;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - point5.x, this.minWidth / 2 - point5.y);
        ctx.lineTo(this.minWidth / 2 - point6.x, this.minWidth / 2 - point6.y);
        ctx.lineTo(this.minWidth / 2 - point1.x, this.minWidth / 2 - point1.y);
        ctx.closePath();
        ctx.fill()
    },
    drawCircle(ctx) {
        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2, // 圆心
            (this.minWidth / 2 - this.padding) / 2, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.fill();
        ctx.fillStyle = "rgb(61, 117, 242)";
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2, // 圆心
            (this.minWidth / 2 - this.padding) / 2 / 6 * 5, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.fill();
    },
    getPoint(radius, angle) {
        var x = radius * Math.cos(angle * Math.PI / 180);
        var y = radius * Math.sin(angle * Math.PI / 180);
        return new Point(x, y);
    },
    startAnim() {
        this.controller.startViewAnim(1500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

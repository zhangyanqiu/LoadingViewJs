/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVWifiController} from '../../component/LVWifi/LVWifiController.js'

export default {
    props: {
        controller: {
            default: new LVWifiController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        signalSize: 4
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })
        ctx.lineWidth = this.minWidth / this.signalSize / 8
        ctx.strokeStyle = this.controller.getViewColor();
        ctx.fillStyle = this.controller.getViewColor();

        var signalRadius = this.minWidth / 2 / this.signalSize;

        if (this.controller.getAnimValue() == 0) { // 初始状态绘制
            for (var i = 0; i < this.signalSize; i++) {
                var radius = signalRadius * (i + 1);
                if (i == 0) {
                    this.drawFirst(ctx, radius);
                } else {
                    this.drawOthers(ctx, radius);
                }
            }
        } else { // 动画开始后绘制
            var item = parseInt(this.controller.getAnimValue() / (1 / this.signalSize)) + 1
            var radius = signalRadius * item;
            if (item == 1) {
                // 忽略上一次的绘制结果，只显示下面绘制的内容
                ctx.globalCompositeOperation = 'copy';
                this.drawFirst(ctx, radius);
                // 切换回默认显示模式，否则上面绘制的图像将不显示
                ctx.globalCompositeOperation = 'source-over';
            } else {
                this.drawOthers(ctx, radius);
            }
        }
    },
    drawFirst(ctx, radius) {
        ctx.beginPath();
        ctx.moveTo(this.compWidth / 2, this.compHeight / 2 + this.minWidth / this.signalSize);
        ctx.arc(this.compWidth / 2, this.compHeight / 2 + this.minWidth / this.signalSize,
            radius,
            -135 / 360 * Math.PI * 2, (-45) / 360 * Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    },
    drawOthers(ctx, radius) {
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2 + this.minWidth / this.signalSize,
            radius,
            -135 / 360 * Math.PI * 2, (-45) / 360 * Math.PI * 2);
        ctx.stroke();
    },
    startAnim() {
        this.controller.startViewAnim(1500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

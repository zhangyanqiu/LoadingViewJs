/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBase} from '../base/LVBase.js'

export class LVImgController extends LVBase {
    startAngle = 0;
    imgPath = '';

    getStartAngle() {
        return this.startAngle;
    }

    getImgPath() {
        return this.imgPath;
    }

    setImgPath(imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    OnAnimationUpdate(value) {
        this.startAngle = value * 360;
    }
}
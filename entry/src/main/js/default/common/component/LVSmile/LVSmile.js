/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVSmileController} from '../../component/LVSmile/LVSmileController.js'

export default {
    props: {
        controller: {
            default: new LVSmileController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 0,
        mEyeWidth: 0,
        isSmile: true,
        startAngle: 0
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        } else {
            this.padding = this.minWidth / 5
        }
        this.mEyeWidth = this.minWidth / 15
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })

        if (this.controller.getAnimValue() < 0.5 && this.controller.getAnimValue() > 0) {
            this.isSmile = false;
            this.startAngle = 720 * this.controller.getAnimValue();
        } else {
            this.isSmile = true;
            this.startAngle = 720;
        }

        ctx.strokeStyle = this.controller.getViewColor();
        ctx.fillStyle = this.controller.getViewColor();
        ctx.lineCap = 'round'
        ctx.lineWidth = this.minWidth / 25;

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        if (this.isSmile) {
            ctx.beginPath();
            ctx.arc((this.compWidth - this.minWidth) / 2 + this.padding + this.mEyeWidth * 1.5, (this.compHeight - this.minWidth) / 2 + this.compHeight / 3, // 圆心
                this.mEyeWidth, // 半径
                0, Math.PI * 2); // 起始点和绘制弧度
            ctx.moveTo(this.compWidth - this.padding - this.mEyeWidth * 0.5 - (this.compWidth - this.minWidth) / 2, (this.compHeight - this.minWidth) / 2 + this.compHeight / 3,)
            ctx.arc(this.compWidth - this.padding - this.mEyeWidth * 1.5 - (this.compWidth - this.minWidth) / 2, (this.compHeight - this.minWidth) / 2 + this.compHeight / 3, // 圆心
                this.mEyeWidth, // 半径
                0, Math.PI * 2); // 起始点和绘制弧度
            ctx.fill();
            // 切换回默认显示模式，否则上面绘制的图像将不显示
            ctx.globalCompositeOperation = 'source-over';
        }
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 2 - this.padding - ctx.lineWidth / 2,
            this.startAngle / 360 * Math.PI * 2, (this.startAngle + 180) / 360 * Math.PI * 2);
        ctx.stroke();

    },
    startAnim() {
        this.controller.startViewAnim(1000, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

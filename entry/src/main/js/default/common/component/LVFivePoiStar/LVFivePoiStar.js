/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVFivePoiStarController} from '../../component/LVFivePoiStar/LVFivePoiStarController.js'
import {Point} from '../../component/base/Point.js'

export default {
    props: {
        controller: {
            default: new LVFivePoiStarController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 5,
        hornCount: 5,
        listPoint: null,
        lastAnimValue: 0
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    getPoint(radius, angle) {
        var x = (radius) * Math.cos(angle * Math.PI / 180) - (this.compWidth - this.minWidth) / 2;
        var y = (radius) * Math.sin(angle * Math.PI / 180) - (this.compHeight - this.minWidth) / 2;
        var p = new Point(x, y);
        return p;
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        if (this.listPoint == null) {
            this.listPoint = new Array()
            for (var i = 0; i < this.hornCount; i++) {
                var p = this.getPoint(this.minWidth / 2 - this.padding, (90 - 360 / this.hornCount) + 360 / this.hornCount * i)
                this.listPoint[i] = p
            }
        }
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const canvas = this.$refs.canvas.getContext('2d', {
            antialias: true
        })
        canvas.lineWidth = 3

        var mAnimatedValue = this.controller.getAnimValue();
        if (this.lastAnimValue == 0 && mAnimatedValue != 0) { // 说明动画刚开始，需要清除默认显示的五角星
            // 忽略上一次的绘制结果，只显示下面绘制的内容
            canvas.globalCompositeOperation = 'copy';
        } else if (mAnimatedValue < this.lastAnimValue) { // 说明开始了新一轮的动画，需要清除上次动画留下的痕迹
            // 忽略上一次的绘制结果，只显示下面绘制的内容
            canvas.globalCompositeOperation = 'copy';
        } else { // 说明动画执行中，需要保留绘制痕迹
            // 切换回默认显示模式，否则上面绘制的图像将不显示
            canvas.globalCompositeOperation = 'source-over';
        }
        this.lastAnimValue = mAnimatedValue
        var cp;
        var currenttime = (mAnimatedValue * 10 - parseInt(mAnimatedValue * 10));
        if (mAnimatedValue == 0) {
            this.drawPathEdge(canvas, this.listPoint[0], this.listPoint[2], this.minWidth);
            this.drawPathEdge(canvas, this.listPoint[2], this.listPoint[4], this.minWidth);
            this.drawPathEdge(canvas, this.listPoint[4], this.listPoint[1], this.minWidth);
            this.drawPathEdge(canvas, this.listPoint[1], this.listPoint[3], this.minWidth);
            this.drawPathEdge(canvas, this.listPoint[3], this.listPoint[0], this.minWidth);
            canvas.beginPath();
            canvas.strokeStyle = this.controller.getCircleColor();
            canvas.arc(this.compWidth / 2, this.compHeight / 2,
                this.minWidth / 2 - canvas.lineWidth / 2 - this.padding,
                0, Math.PI * 2);
            canvas.stroke();
        } else if (mAnimatedValue > 0 && mAnimatedValue <= 0.1) {
            cp = this.drawOneEdge(currenttime, 1, this.listPoint[0], this.listPoint[2]);
            this.drawPathEdge(canvas, this.listPoint[0], cp, this.minWidth);
        } else if (mAnimatedValue > 0.1 && mAnimatedValue <= 0.2) {
            this.drawPathEdge(canvas, this.listPoint[0], this.listPoint[2], this.minWidth);
            cp = this.drawOneEdge(currenttime, 1, this.listPoint[2], this.listPoint[4]);
            this.drawPathEdge(canvas, this.listPoint[2], cp, this.minWidth);
        } else if (mAnimatedValue > 0.2 && mAnimatedValue <= 0.3) {
            this.drawPathEdge(canvas, this.listPoint[2], this.listPoint[4], this.minWidth);
            cp = this.drawOneEdge(currenttime, 1, this.listPoint[4], this.listPoint[1]);
            this.drawPathEdge(canvas, this.listPoint[4], cp, this.minWidth);
        } else if (mAnimatedValue > 0.3 && mAnimatedValue <= 0.4) {
            this.drawPathEdge(canvas, this.listPoint[4], this.listPoint[1], this.minWidth);
            cp = this.drawOneEdge(currenttime, 1, this.listPoint[1], this.listPoint[3]);
            this.drawPathEdge(canvas, this.listPoint[1], cp, this.minWidth);
        } else if (mAnimatedValue > 0.4 && mAnimatedValue <= 0.5) {
            this.drawPathEdge(canvas, this.listPoint[1], this.listPoint[3], this.minWidth);
            cp = this.drawOneEdge(currenttime, 1, this.listPoint[3], this.listPoint[0]);
            this.drawPathEdge(canvas, this.listPoint[3], cp, this.minWidth);
        } else if (mAnimatedValue > 0.5 && mAnimatedValue <= 0.75) {
            this.drawPathEdge(canvas, this.listPoint[3], this.listPoint[0], this.minWidth);
            canvas.beginPath();
            canvas.strokeStyle = this.controller.getCircleColor();
            canvas.arc(this.compWidth / 2, this.compHeight / 2,
                this.minWidth / 2 - canvas.lineWidth / 2 - this.padding,
                (-180 + ((90 - 360 / this.hornCount))) / 360 * Math.PI * 2, (360 / 0.25 * (mAnimatedValue - 0.5)) / 360 * Math.PI * 2);
            canvas.stroke();
        } else {
            canvas.beginPath();
            canvas.lineWidth = 5
            canvas.strokeStyle = this.controller.getCircleColor();
            canvas.arc(this.compWidth / 2, this.compHeight / 2,
                this.minWidth / 2 - canvas.lineWidth / 2 - this.padding,
                0, Math.PI * 2);
            canvas.stroke();
        }

    },
    drawOneEdge(currenttime, alltime, startP, endP) {
        var x = startP.getX() - (startP.getX() - endP.getX()) / alltime * currenttime;
        var y = startP.getY() - (startP.getY() - endP.getY()) / alltime * currenttime;
        var resPoint = new Point(x, y);
        return resPoint;
    },
    drawPathEdge(canvas, start, end, mWidth) {
        canvas.beginPath();
        canvas.strokeStyle = this.controller.getViewColor();
        canvas.moveTo(mWidth / 2 - start.getX(), mWidth / 2 - start.getY());
        canvas.lineTo(mWidth / 2 - end.getX(), mWidth / 2 - end.getY());
        canvas.stroke();
    },
    startAnim() {
        this.controller.startViewAnim(3500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

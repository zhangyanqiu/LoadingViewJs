/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBlockController} from '../../component/LVBlock/LVBlockController.js'

export default {
    props: {
        controller: {
            default: new LVBlockController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        rhomboidsX: 0,
        rhomboidsY: 0,
        moveYtoCenter: 0,
        viewColor: "rgb(247,202,42)",
        viewColorLeft: "rgb(227,144,11)",
        viewColorRight: "rgb(188,91,26)",
        shadowColor: "rgb(0,0,0)",
    },
    onAttached() {
        this.viewColor = this.controller.getViewColor()
        this.viewColorLeft = this.controller.getViewColorLeft()
        this.viewColorRight = this.controller.getViewColorRight()
        this.controller.setAnimValue(0, 3)
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.rhomboidsX = 3 * this.minWidth / 16 / Math.sqrt(3);
        this.rhomboidsY = this.minWidth / 16;
        if (!this.controller.getIsShadow()) {
            this.moveYtoCenter = this.minWidth / 4;
        } else {
            this.moveYtoCenter = 0;
        }
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        if (this.controller.getAnimStep() == 0) {
            this.drawStep1(ctx, this.controller.getAnimValue());
            if (this.controller.getIsShadow()) {
                this.drawShadowStep1(ctx, this.controller.getAnimValue());
            }
        } else if (this.controller.getAnimStep() == 1) {
            this.drawStep2(ctx, this.controller.getAnimValue());
            if (this.controller.getIsShadow()) {
                this.drawShadowStep2(ctx, this.controller.getAnimValue());
            }
        } else {
            this.drawStep3(ctx, this.controller.getAnimValue());
            if (this.controller.getIsShadow()) {
                this.drawShadowStep3(ctx, this.controller.getAnimValue());
            }
        }

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    drawStep1(ctx, animValue) {
        var moveX = this.rhomboidsX / 2.0 * animValue;
        var moveY = this.rhomboidsY / 2.0 * animValue;

        ctx.fillStyle = this.viewColorLeft;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - moveX, this.rhomboidsY * 12 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 - moveX, this.rhomboidsY * 12 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 12 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 12 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';

        ctx.fillStyle = this.viewColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - moveX, this.rhomboidsY * 12 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 11 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - moveX, this.minWidth / 4 * 3 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 11 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 12 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 11 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.minWidth / 4 * 3 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 11 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();

        ctx.fillStyle = this.viewColorRight;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.minWidth / 4 * 3 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.minWidth / 4 * 3 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();
    },
    drawStep2(ctx, animValue) {
        var moveX = this.rhomboidsX * animValue;
        var moveY = this.rhomboidsY * animValue;

        ctx.fillStyle = this.viewColorRight;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();

        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';

        ctx.fillStyle = this.viewColorLeft;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();

        ctx.fillStyle = this.viewColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 11 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 12 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 11 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 11 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 11 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();

    },
    drawStep3(ctx, animValue) {
        var moveX = this.rhomboidsX / 2.0 * animValue;
        var moveY = this.rhomboidsY / 2.0 * animValue;

        ctx.fillStyle = this.viewColorLeft;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';

        ctx.fillStyle = this.viewColorRight;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.rhomboidsY * 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();

        ctx.fillStyle = this.viewColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 11 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.minWidth / 4 * 3 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - this.rhomboidsY + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 11 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 + moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 11 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 11 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - moveY - this.minWidth / 2 + this.moveYtoCenter);
        ctx.closePath();
        ctx.fill();
    },
    drawShadowStep1(ctx, animValue) {
        var moveX = this.rhomboidsX / 2.0 * animValue;
        var moveY = this.rhomboidsY / 2.0 * animValue;

        ctx.fillStyle = this.shadowColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - moveX, this.rhomboidsY * 12 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 11 - moveY);
        ctx.lineTo(this.minWidth / 2 - moveX, this.minWidth / 4 * 3 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 - moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 11 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY + moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 12 + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 11 + moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + moveX, this.minWidth / 4 * 3 + moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + moveX, this.rhomboidsY * 13 + moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 11 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY - moveY);
        ctx.closePath();
        ctx.fill()
    },
    drawShadowStep2(ctx, animValue) {
        var moveX = this.rhomboidsX * animValue;
        var moveY = this.rhomboidsY * animValue;

        ctx.fillStyle = this.shadowColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 11 - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 12 - this.rhomboidsY + this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 11 - this.rhomboidsY + this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 11 + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 11 + this.rhomboidsY - this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2);
        ctx.closePath();
        ctx.fill()
    },
    drawShadowStep3(ctx, animValue) {
        var moveX = this.rhomboidsX / 2.0 * animValue;
        var moveY = this.rhomboidsY / 2.0 * animValue;

        ctx.fillStyle = this.shadowColor;
        ctx.beginPath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 12 - this.rhomboidsY / 2 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 11 - this.rhomboidsY / 2 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.minWidth / 4 * 3 - this.rhomboidsY / 2 - this.rhomboidsY + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX - this.rhomboidsX / 2 + this.rhomboidsX + moveX, this.rhomboidsY * 13 - this.rhomboidsY / 2 - this.rhomboidsY + moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 12 - this.rhomboidsY + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 11 - this.rhomboidsY + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.minWidth / 4 * 3 - this.rhomboidsY + this.rhomboidsY / 2 + moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 + moveX, this.rhomboidsY * 13 - this.rhomboidsY + this.rhomboidsY / 2 + moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 12 + this.rhomboidsY / 2 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 11 + this.rhomboidsY / 2 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.minWidth / 4 * 3 + this.rhomboidsY / 2 + this.rhomboidsY - moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX + this.rhomboidsX + this.rhomboidsX / 2 - this.rhomboidsX - moveX, this.rhomboidsY * 13 + this.rhomboidsY / 2 + this.rhomboidsY - moveY);
        ctx.closePath();
        ctx.moveTo(this.minWidth / 2 - this.rhomboidsX * 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 12 + this.rhomboidsY - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 - this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 11 + this.rhomboidsY - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.minWidth / 4 * 3 + this.rhomboidsY - this.rhomboidsY / 2 - moveY);
        ctx.lineTo(this.minWidth / 2 + -this.rhomboidsX + this.rhomboidsX - this.rhomboidsX / 2 - moveX, this.rhomboidsY * 13 + this.rhomboidsY - this.rhomboidsY / 2 - moveY);
        ctx.closePath();
        ctx.fill()
    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBase} from '../base/LVBase.js'

export class LVBlockController extends LVBase {
    animValue = 0;
    viewColor = 'rgb(247,202,42)';
    viewColorLeft = 'rgb(227,144,11)';
    viewColorRight = 'rgb(188,91,26)';
    shadowColor = 'rgb(0,0,0)';
    animStep = 0;
    mShadow = true;

    getAnimValue() {
        return this.animValue;
    }

    getAnimStep() {
        return this.animStep;
    }

    getViewColor() {
        return this.viewColor;
    }

    getViewColorLeft() {
        return this.viewColorLeft;
    }

    getViewColorRight() {
        return this.viewColorRight;
    }

    setViewColor(viewColor) {
        if (viewColor.substr(0, 1) == "#") {
            viewColor = this.hexToRgb(viewColor)
        }
        var rgbs = this.getRgb(viewColor)
        var red = rgbs[0]
        var green = rgbs[1]
        var blue = rgbs[2]

        this.viewColor = viewColor;
        this.viewColorLeft = "rgb(" + ((red - 20) > 0 ? red - 20 : 0) + "," + ((green - 58) > 0 ? green - 58 : 0) + "," + ((blue - 31) > 0 ? blue - 31 : 0) + ")";
        this.viewColorRight = "rgb(" + ((red - 59) > 0 ? red - 59 : 0) + "," + ((green - 111) > 0 ? green - 111 : 0) + "," + ((blue - 16) > 0 ? blue - 16 : 0) + ")";
        return this;
    }

    hexToRgb(hexColor) {
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        if (hexColor && reg.test(hexColor)) {
            if (hexColor.length === 4) {
                var sColorNew = "#";
                for (var i = 1; i < 4; i += 1) {
                    sColorNew += hexColor.slice(i, i + 1).concat(hexColor.slice(i, i + 1));
                }
                hexColor = sColorNew;
            }
            //处理六位的颜色值
            var sColorChange = [];
            for (var i = 1; i < 7; i += 2) {
                sColorChange.push(parseInt("0x" + hexColor.slice(i, i + 2)));
            }
            return "rgb(" + sColorChange.join(",") + ")";
        } else {
            return hexColor;
        }
    }

    getRgb(rgbColor) {
        return rgbColor.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
    }

    getShadowColor() {
        return this.shadowColor;
    }

    setShadowColor(shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    getIsShadow() {
        return this.mShadow;
    }

    isShadow(show) {
        this.mShadow = show;
        return this;
    }

    OnAnimationUpdate(value) {
        this.animStep = parseInt(value)
        this.animValue = value - this.animStep;
    }
}
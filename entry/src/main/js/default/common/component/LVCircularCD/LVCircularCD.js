/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVCircularCDController} from '../../component/LVCircularCD/LVCircularCDController.js'

export default {
    props: {
        controller: {
            default: new LVCircularCDController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 5
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        // 页面的第一个控件需要做延迟，否则宽高有一定概率获取不到
        setTimeout(() => {
            this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
            this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
            this.minWidth = Math.min(this.compWidth, this.compHeight)
            this.onDraw()
        }, 50)
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        // 外圈
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = this.controller.getViewColor();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 2 - ctx.lineWidth / 2 - this.padding,
            0, Math.PI * 2);
        ctx.stroke();
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        // 中心小圆
        ctx.beginPath();
        ctx.lineWidth = 3; //圆环宽度
        ctx.arc(this.compWidth / 2, this.compHeight / 2, // 圆心
            5, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.stroke();
        // 中间的弧
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 3 - ctx.lineWidth / 2 - this.padding,
            this.controller.getAnimValue() / 360 * Math.PI * 2, (this.controller.getAnimValue() + 80) / 360 * Math.PI * 2);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 3 - ctx.lineWidth / 2 - this.padding,
            (this.controller.getAnimValue() + 180) / 360 * Math.PI * 2, (this.controller.getAnimValue() + 260) / 360 * Math.PI * 2);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 4 - ctx.lineWidth / 2 - this.padding,
            this.controller.getAnimValue() / 360 * Math.PI * 2, (this.controller.getAnimValue() + 80) / 360 * Math.PI * 2);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 4 - ctx.lineWidth / 2 - this.padding,
            (this.controller.getAnimValue() + 180) / 360 * Math.PI * 2, (this.controller.getAnimValue() + 260) / 360 * Math.PI * 2);
        ctx.stroke();

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    startAnim() {
        this.controller.startViewAnim(1500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVCircularJumpController} from '../../component/LVCircularJump/LVCircularJumpController.js'

export default {
    props: {
        controller: {
            default: new LVCircularJumpController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        padding: 0,
        circularCount: 4,
        circularRadius: 6
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
        this.controller.setAnimValue(0, this.circularCount)
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })
        ctx.fillStyle = this.controller.getViewColor();

        var circularX = (this.compWidth - this.padding * 2) / this.circularCount;
        if (this.controller.getAnimTimes() == 0 && this.controller.getAnimValue() == 0) {
            // 忽略上一次的绘制结果，只显示下面绘制的内容
            ctx.globalCompositeOperation = 'copy';
            for (var i = 0; i < this.circularCount; i++) {
                if (i == 1) {
                    // 忽略上一次的绘制结果，只显示下面绘制的内容
                    ctx.globalCompositeOperation = 'source-over';
                }
                ctx.beginPath();
                ctx.arc(i * circularX + circularX / 2 + this.padding, this.compHeight / 2,
                    this.circularRadius,
                    0, Math.PI * 2);
                ctx.fill();
            }
        } else {
            for (var i = 0; i < this.circularCount; i++) {
                if (i == this.controller.getAnimTimes() % this.circularCount) {
                    ctx.clearRect(i * circularX + circularX / 2 + this.padding - this.circularRadius, 0,
                        this.circularRadius * 2, this.compHeight)
                    ctx.beginPath();
                    ctx.arc(i * circularX + circularX / 2 + this.padding, this.compHeight / 2 - this.compHeight / 2 * this.controller.getAnimValue(),
                        this.circularRadius,
                        0, Math.PI * 2);
                    ctx.fill();
                }
            }
        }
    },
    startAnim() {
        this.controller.startViewAnim(500 * this.circularCount, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVCircularController} from '../../component/LVCircular/LVCircularController.js'

export default {
    props: {
        controller: {
            default: new LVCircularController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 0,
        roundRadius: 4,
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        this.roundRadius = this.minWidth / 30
        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.beginPath();
        ctx.fillStyle = this.controller.getRoundColor();
        for (var i = 0; i < 9; i++) {
            var x2 = (this.compWidth / 2 - this.padding - this.roundRadius) * Math.cos((this.controller.getAnimValue() + 45 * i) * Math.PI / 180)
            var y2 = (this.compHeight / 2 - this.padding - this.roundRadius) * Math.sin((this.controller.getAnimValue() + 45 * i) * Math.PI / 180)
            ctx.moveTo(this.compWidth / 2 - x2 + this.roundRadius, this.compHeight / 2 - y2)
            ctx.arc(this.compWidth / 2 - x2, this.compHeight / 2 - y2, // 圆心
                this.roundRadius, // 半径
                0, Math.PI * 2); // 起始点和绘制弧度
        }
        ctx.fill();
        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        ctx.fillStyle = this.controller.getViewColor();
        ctx.arc(this.compWidth / 2, this.compHeight / 2, // 圆心
            this.minWidth / 2 - this.padding - this.roundRadius * 6, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.fill();

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    startAnim() {
        this.controller.startViewAnim(3500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVGhostController} from '../../component/LVGhost/LVGhostController.js'
import {Rect} from '../../component/base/Rect.js'

export default {
    props: {
        controller: {
            default: new LVGhostController()
        }
    },
    data: {
        visable1: "hidden",
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        padding: 10,
        mskirtH: 0,
        wspace: 10,
        hspace: 10,
        viewColor: "white",
        eyesColor: "#DC000000",
        shadowColor: "#3C000000",
        rectFGhost: new Rect(),
        rectFGhostShadow: new Rect(),
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
        this.viewColor = this.controller.getViewColor()
        this.eyesColor = this.controller.getEyesColor()
        this.controller.setAnimRepeatMode(true)
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.mskirtH = parseInt(this.compWidth / 40)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        if (!this.controller.getAnimIsRunning()) {
            this.wspace = 10
        } else if (this.controller.getIsBack()) {
            this.wspace = 22
        } else {
            this.wspace = -2
        }
        var mAnimatedValue = this.controller.getAnimValue()
        var distance = (this.compWidth - 2 * this.padding) / 3 * 2 * mAnimatedValue;
        this.rectFGhost.left = this.padding + distance;
        this.rectFGhost.right = (this.compWidth - 2 * this.padding) / 3 + distance;
        var moveY;
        var moveYMax = this.compHeight / 4 / 2;
        var shadowHighMax = 5;
        var shadowHigh = 0;
        if (mAnimatedValue <= 0.25) {
            moveY = moveYMax / 0.25 * mAnimatedValue;
            this.rectFGhost.top = moveY;
            this.rectFGhost.bottom = this.compHeight / 4 * 3 + moveY;
            shadowHigh = shadowHighMax / 0.25 * mAnimatedValue;
        } else if (mAnimatedValue > 0.25 && mAnimatedValue <= 0.5) {
            moveY = moveYMax / 0.25 * (mAnimatedValue - 0.25);
            this.rectFGhost.top = moveYMax - moveY;
            this.rectFGhost.bottom = this.compHeight / 4 * 3 + moveYMax - moveY;
            shadowHigh = shadowHighMax - shadowHighMax / 0.25 * (mAnimatedValue - 0.25);
        } else if (mAnimatedValue > 0.5 && mAnimatedValue <= 0.75) {
            moveY = moveYMax / 0.25 * (mAnimatedValue - 0.5);
            this.rectFGhost.top = moveY;
            this.rectFGhost.bottom = this.compHeight / 4 * 3 + moveY;
            shadowHigh = shadowHighMax / 0.25 * (mAnimatedValue - 0.5);
        } else if (mAnimatedValue > 0.75 && mAnimatedValue <= 1) {
            moveY = moveYMax / 0.25 * (mAnimatedValue - 0.75);
            this.rectFGhost.top = moveYMax - moveY;
            this.rectFGhost.bottom = this.compHeight / 4 * 3 + moveYMax - moveY;
            shadowHigh = shadowHighMax - shadowHighMax / 0.25 * (mAnimatedValue - 0.75);
        }
        this.rectFGhostShadow.top = this.compHeight - 25 + shadowHigh;
        this.rectFGhostShadow.bottom = this.compHeight - 5 - shadowHigh;
        this.rectFGhostShadow.left = this.rectFGhost.left + 5 + shadowHigh * 3;
        this.rectFGhostShadow.right = this.rectFGhost.right - 5 - shadowHigh * 3;

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        this.drawShadow(ctx);
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        this.drawHead(ctx);
        this.drawBody(ctx);
        this.drawEyes(ctx);

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable1 = "hidden"
            this.visable2 = "visible"
        } else {
            this.visable1 = "visible"
            this.visable2 = "hidden"
        }
    },
    drawShadow(ctx) {
        ctx.fillStyle = this.shadowColor
        ctx.beginPath();
        ctx.ellipse(this.rectFGhostShadow.getCenterX(), this.rectFGhostShadow.getCenterY(),
            this.rectFGhostShadow.getWidth() / 2, this.rectFGhostShadow.getHeight() / 2,
            0, 0, Math.PI * 2, 0);
        ctx.fill();
    },
    drawHead(ctx) {
        ctx.fillStyle = this.viewColor
        ctx.beginPath();
        ctx.arc(this.rectFGhost.getCenterX(), this.rectFGhost.getWidth() / 2 + this.rectFGhost.top,
            this.rectFGhost.getWidth() / 2 - 15,
            0, Math.PI * 2);
    },
    drawBody(ctx) {
        var x = (this.rectFGhost.getWidth() / 2 - 15) * Math.cos(5 * Math.PI / 180);
        var y = (this.rectFGhost.getWidth() / 2 - 15) * Math.sin(5 * Math.PI / 180);
        var x2 = (this.rectFGhost.getWidth() / 2 - 15) * Math.cos(175 * Math.PI / 180);
        var y2 = (this.rectFGhost.getWidth() / 2 - 15) * Math.sin(175 * Math.PI / 180);

        ctx.moveTo(this.rectFGhost.getCenterX() - x, this.rectFGhost.getWidth() / 2 - y + this.rectFGhost.top);
        ctx.lineTo(this.rectFGhost.getCenterX() - x2, this.rectFGhost.getWidth() / 2 - y2 + this.rectFGhost.top);
        ctx.quadraticCurveTo(this.rectFGhost.right + this.wspace / 2, this.rectFGhost.bottom,
            this.rectFGhost.right - this.wspace, this.rectFGhost.bottom - this.hspace);
        var m = (this.rectFGhost.getWidth() - 2 * this.wspace) / 7;
        for (var i = 0; i < 7; i++) {
            if (i % 2 == 0) {
                ctx.quadraticCurveTo(this.rectFGhost.right - this.wspace - m * i - (m / 2), this.rectFGhost.bottom - this.hspace - this.mskirtH,
                    this.rectFGhost.right - this.wspace - (m * (i + 1)), this.rectFGhost.bottom - this.hspace);
            } else {
                ctx.quadraticCurveTo(this.rectFGhost.right - this.wspace - m * i - (m / 2), this.rectFGhost.bottom - this.hspace + this.mskirtH,
                    this.rectFGhost.right - this.wspace - (m * (i + 1)), this.rectFGhost.bottom - this.hspace);
            }
        }
        ctx.quadraticCurveTo(this.rectFGhost.left - 5, this.rectFGhost.bottom,
            this.rectFGhost.left + this.rectFGhost.getWidth() / 2 - x, this.rectFGhost.getWidth() / 2 - y + this.rectFGhost.top);
        ctx.closePath();
        ctx.fill();
    },
    drawEyes(ctx) {
        ctx.fillStyle = this.eyesColor
        ctx.beginPath();
        ctx.arc(this.rectFGhost.getCenterX() - this.mskirtH * 3 / 2 + this.mskirtH * (this.controller.getIsBack() ? -1 : 1),
            this.rectFGhost.getWidth() / 2 + this.mskirtH + this.rectFGhost.top,
            this.mskirtH * 0.9, 0, Math.PI * 2);
        ctx.arc(this.rectFGhost.left + this.rectFGhost.getWidth() / 2 + this.mskirtH * 3 / 2 + this.mskirtH * (this.controller.getIsBack() ? -1 : 1),
            this.rectFGhost.getWidth() / 2 + this.mskirtH + this.rectFGhost.top,
            this.mskirtH * 0.9, 0, Math.PI * 2);
        ctx.fill();
    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

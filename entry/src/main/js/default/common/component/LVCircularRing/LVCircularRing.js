/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVCircularRingController} from '../../component/LVCircularRing/LVCircularRingController.js'

export default {
    props: {
        controller: {
            default: new LVCircularRingController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 5
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.beginPath();
        ctx.lineWidth = 8; //圆环宽度
        ctx.strokeStyle = this.controller.getViewColor();
        ctx.arc(this.compWidth / 2, this.compHeight / 2, // 圆心
            this.minWidth / 2 - ctx.lineWidth / 2 - this.padding, // 半径
            0, Math.PI * 2); // 起始点和绘制弧度
        ctx.stroke();
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        ctx.lineWidth = 8;
        ctx.strokeStyle = this.controller.getBarColor();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 2 - ctx.lineWidth / 2 - this.padding,
            this.controller.getAnimValue() / 360 * Math.PI * 2, (this.controller.getAnimValue() + 100) / 360 * Math.PI * 2);
        ctx.stroke();

    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

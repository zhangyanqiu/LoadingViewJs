/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVGearsController} from '../../component/LVGears/LVGearsController.js'

export default {
    props: {
        controller: {
            default: new LVGearsController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 0,
        mWheelSmallLength: 0, // 小齿轮的齿的长度
        mWheelBigLength: 0, // 大齿轮的齿的长度
        mWheelSmallSpace: 8, // 小齿轮的齿的间隔度数
        mWheelBigSpace: 6, // 大齿轮的齿的间隔度数
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        } else {
            this.padding = this.minWidth / 10
        }
        this.mWheelSmallLength = this.minWidth / 16
        this.mWheelBigLength = this.minWidth / 20
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        ctx.strokeStyle = this.controller.getViewColor();
        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        this.drawWheelBig(ctx)
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        this.drawWheelSmall(ctx)
        this.drawCircleAndAxle(ctx)

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    drawWheelBig(ctx) {
        ctx.lineWidth = 3;
        ctx.beginPath();
        for (var i = 0; i < 360; i = i + this.mWheelBigSpace) {
            var angle = this.controller.getAnimValue() * this.mWheelBigSpace + i; // clockwise 顺时针
            var x = (this.minWidth / 2 - this.padding + this.mWheelBigLength) * Math.cos(angle * Math.PI / 180);
            var y = (this.minWidth / 2 - this.padding + this.mWheelBigLength) * Math.sin(angle * Math.PI / 180);
            var x2 = (this.minWidth / 2 - this.padding) * Math.cos(angle * Math.PI / 180);
            var y2 = (this.minWidth / 2 - this.padding) * Math.sin(angle * Math.PI / 180);
            ctx.moveTo(this.compWidth / 2 - x, this.compHeight / 2 - y);
            ctx.lineTo(this.compWidth / 2 - x2, this.compHeight / 2 - y2);
        }
        ctx.stroke();
    },
    drawWheelSmall(ctx) {
        ctx.lineWidth = 1.5;
        ctx.beginPath();
        for (var i = 0; i < 360; i = i + this.mWheelSmallSpace) {
            var angle = 360 - this.controller.getAnimValue() * this.mWheelSmallSpace + i; // anticlockwise 逆时针
            var x = (this.minWidth / 4 - this.padding / 2) * Math.cos(angle * Math.PI / 180);
            var y = (this.minWidth / 4 - this.padding / 2) * Math.sin(angle * Math.PI / 180);
            var x2 = (this.minWidth / 4 - this.padding / 2 + this.mWheelSmallLength) * Math.cos(angle * Math.PI / 180);
            var y2 = (this.minWidth / 4 - this.padding / 2 + this.mWheelSmallLength) * Math.sin(angle * Math.PI / 180);
            ctx.moveTo(this.compWidth / 2 - x, this.compHeight / 2 - y);
            ctx.lineTo(this.compWidth / 2 - x2, this.compHeight / 2 - y2);
        }
        ctx.stroke();
    },
    drawCircleAndAxle(ctx) {
        ctx.lineWidth = 6;
        ctx.beginPath();
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 2 - this.padding,
            -Math.PI, Math.PI);
        ctx.moveTo(this.compWidth / 2 - this.minWidth / 4 + this.padding / 2);
        ctx.arc(this.compWidth / 2, this.compHeight / 2,
            this.minWidth / 4 - this.padding / 2,
            -Math.PI, Math.PI);
        for (var i = 0; i < 3; i++) {
            var x = (this.minWidth / 2 - this.padding) * Math.cos(i * (360 / 3) * Math.PI / 180);
            var y = (this.minWidth / 2 - this.padding) * Math.sin(i * (360 / 3) * Math.PI / 180);
            ctx.moveTo(this.compWidth / 2, this.compHeight / 2);
            ctx.lineTo(this.compWidth / 2 - x, this.compHeight / 2 - y);
        }
        ctx.stroke();
    },
    startAnim() {
        this.controller.startViewAnim(1000, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

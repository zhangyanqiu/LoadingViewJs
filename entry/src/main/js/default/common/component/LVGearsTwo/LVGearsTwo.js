/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVGearsTwoController} from '../../component/LVGearsTwo/LVGearsTwoController.js'
import {CanvasRotateUtil} from '../../component/base/CanvasRotateUtil.js'

export default {
    props: {
        controller: {
            default: new LVGearsTwoController()
        }
    },
    data: {
        visable2: "visible",
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 15,
        canvasRotateUtil1: null,
        canvasRotateUtil2: null,
        mWheelLength: 6, // 齿轮的齿的长度
        mWheelSmallSpace: 8, // 小齿轮的齿的间隔度数
        mWheelBigSpace: 6, // 大齿轮的齿的间隔度数
        hypotenuse: 0,
        smallRingCenterX: 0,
        smallRingCenterY: 0,
        bigRingCenterX: 0,
        bigRingCenterY: 0,
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.hypotenuse = this.minWidth * Math.sqrt(2)
        this.smallRingCenterX = (this.hypotenuse / 6) * Math.cos(45 * Math.PI / 180)
        this.smallRingCenterY = (this.hypotenuse / 6) * Math.sin(45 * Math.PI / 180)
        this.bigRingCenterX = (this.hypotenuse / 2) * Math.cos(45 * Math.PI / 180);
        this.bigRingCenterY = (this.hypotenuse / 2) * Math.sin(45 * Math.PI / 180);
        // 初始化画布旋转工具类
        if (this.canvasRotateUtil1 == null) {
            this.canvasRotateUtil1 = new CanvasRotateUtil(this.compWidth, this.compHeight)
            this.canvasRotateUtil2 = new CanvasRotateUtil(this.compWidth, this.compHeight)
        }
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件,使用两层画布，解决绘制的东西闪现的问题
        var ctx = null;
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            ctx = this.$refs.canvas2.getContext('2d', {
                antialias: true
            })
        } else {
            ctx = this.$refs.canvas1.getContext('2d', {
                antialias: true
            })
        }

        var rotate = 180
        // 通过工具类让画布绕中心点旋转
        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.canvasRotateUtil2.rotate(ctx, rotate)
        } else {
            this.canvasRotateUtil1.rotate(ctx, rotate)
        }

        ctx.strokeStyle = this.controller.getViewColor();
        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        this.drawSmallRingAndGear(ctx)
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';
        this.drawBigRingAndGear(ctx)
        this.drawAxle(ctx)

        if (this.controller.getAnimDrawTime() % 2 == 0) {
            this.visable2 = "visible"
        } else {
            this.visable2 = "hidden"
        }
    },
    drawSmallRingAndGear(ctx) {
        ctx.lineWidth = 3;
        ctx.beginPath();
        ctx.arc(this.padding + this.smallRingCenterX, this.padding + this.smallRingCenterY,
            this.smallRingCenterX,
            0, Math.PI * 2);
        var smallCenterX = this.padding + this.smallRingCenterX
        var smallCenterY = this.padding + this.smallRingCenterY
        var smallR = this.smallRingCenterX / 2
        ctx.moveTo(smallCenterX + smallR, smallCenterY);
        ctx.arc(smallCenterX, smallCenterY, smallR, 0, Math.PI * 2);
        for (var i = 0; i < 360; i = i + this.mWheelSmallSpace) {
            var angle = this.controller.getAnimValue() * this.mWheelSmallSpace + i;
            var x3 = (this.smallRingCenterX) * Math.cos(angle * Math.PI / 180);
            var y3 = (this.smallRingCenterY) * Math.sin(angle * Math.PI / 180);
            var x4 = (this.smallRingCenterX + this.mWheelLength) * Math.cos(angle * Math.PI / 180);
            var y4 = (this.smallRingCenterY + this.mWheelLength) * Math.sin(angle * Math.PI / 180);
            ctx.moveTo(this.padding + this.smallRingCenterX - x4, this.smallRingCenterY + this.padding - y4);
            ctx.lineTo(this.smallRingCenterX + this.padding - x3, this.smallRingCenterY + this.padding - y3);
        }
        ctx.stroke();
    },
    drawBigRingAndGear(ctx) {
        ctx.lineWidth = 4;
        var strokeWidth = ctx.lineWidth / 4;
        ctx.beginPath();
        ctx.arc(this.bigRingCenterX + this.padding + this.mWheelLength * 2 + strokeWidth, this.bigRingCenterY + this.padding + this.mWheelLength * 2 + strokeWidth,
            this.bigRingCenterX - this.smallRingCenterX - strokeWidth,
            0, Math.PI * 2);
        var bigCenterX = this.bigRingCenterX + this.padding + this.mWheelLength * 2 + strokeWidth
        var bigCenterY = this.bigRingCenterY + this.padding + this.mWheelLength * 2 + strokeWidth
        var bigR = (this.bigRingCenterX - this.smallRingCenterX) / 2 - strokeWidth
        ctx.moveTo(bigCenterX + bigR, bigCenterY);
        ctx.arc(bigCenterX, bigCenterY, bigR, 0, Math.PI * 2);
        for (var i = 0; i < 360; i = i + this.mWheelBigSpace) {
            var angle = 360 - (this.controller.getAnimValue() * this.mWheelBigSpace + i);
            var x3 = (this.bigRingCenterX - this.smallRingCenterX) * Math.cos(angle * Math.PI / 180);
            var y3 = (this.bigRingCenterY - this.smallRingCenterY) * Math.sin(angle * Math.PI / 180);
            var x4 = (this.bigRingCenterX - this.smallRingCenterX + this.mWheelLength) * Math.cos(angle * Math.PI / 180);
            var y4 = (this.bigRingCenterY - this.smallRingCenterY + this.mWheelLength) * Math.sin(angle * Math.PI / 180);
            ctx.moveTo(this.bigRingCenterX + this.padding - x4 + this.mWheelLength * 2 + strokeWidth, this.bigRingCenterY + this.padding - y4 + this.mWheelLength * 2 + strokeWidth);
            ctx.lineTo(this.bigRingCenterX + this.padding - x3 + this.mWheelLength * 2 + strokeWidth, this.bigRingCenterY + this.padding - y3 + this.mWheelLength * 2 + strokeWidth);
        }
        ctx.stroke();
    },
    drawAxle(ctx) {
        ctx.lineWidth = 4;
        ctx.beginPath();
        for (var i = 0; i < 3; i++) {
            var x3 = (this.smallRingCenterX) * Math.cos(i * (360 / 3) * Math.PI / 180);
            var y3 = (this.smallRingCenterY) * Math.sin(i * (360 / 3) * Math.PI / 180);
            ctx.moveTo(this.padding + this.smallRingCenterX, this.padding + this.smallRingCenterY);
            ctx.lineTo(this.padding + this.smallRingCenterX - x3, this.padding + this.smallRingCenterY - y3);
        }
        for (var i = 0; i < 3; i++) {
            var x3 = (this.bigRingCenterX - this.smallRingCenterX) * Math.cos(i * (360 / 3) * Math.PI / 180);
            var y3 = (this.bigRingCenterY - this.smallRingCenterY) * Math.sin(i * (360 / 3) * Math.PI / 180);
            ctx.moveTo(this.bigRingCenterX + this.padding + this.mWheelLength * 2, this.bigRingCenterY + this.padding + this.mWheelLength * 2);
            ctx.lineTo(this.bigRingCenterX + this.padding + this.mWheelLength * 2 - x3, this.bigRingCenterY + this.padding + this.mWheelLength * 2 - y3);
        }
        ctx.stroke();
    },
    startAnim() {
        this.controller.startViewAnim(1000, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVLineWithTextController} from '../../component/LVLineWithText/LVLineWithTextController.js'

export default {
    props: {
        controller: {
            default: new LVLineWithTextController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        padding: 5,
        lineWidth: 3,
        textSize: 30,
        mVlaue: 0
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
        if (this.controller.getLineWidth() != -1) {
            this.lineWidth = this.controller.getLineWidth()
        }
        if (this.controller.getTextSize() != -1) {
            this.textSize = this.controller.getTextSize()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })
        ctx.globalCompositeOperation = 'copy';
        ctx.strokeStyle = this.controller.getViewColor()
        ctx.fillStyle = this.controller.getTextColor();
        ctx.lineWidth = this.lineWidth;
        ctx.textBaseline = 'middle';
        ctx.font = this.textSize + 'px sans-serif';

        var text = this.mVlaue + "%";
        var textWidth = ctx.measureText(text).width;

        if (this.mVlaue == 0) {
            ctx.beginPath()
            ctx.moveTo(this.padding + textWidth, this.compHeight / 2);
            ctx.lineTo(this.compWidth - this.padding, this.compHeight / 2);
            ctx.stroke();
            ctx.fillText(text, this.padding, this.compHeight / 2);
        } else if (this.mVlaue >= 100) {
            ctx.beginPath()
            ctx.moveTo(this.padding, this.compHeight / 2);
            ctx.lineTo(this.compWidth - this.padding - textWidth, this.compHeight / 2);
            ctx.stroke();
            ctx.fillText(text, this.compWidth - this.padding - textWidth, this.compHeight / 2);
        } else {
            var width = this.compWidth - 2 * this.padding - textWidth;
            ctx.beginPath()
            ctx.moveTo(this.padding, this.compHeight / 2);
            ctx.lineTo(parseInt(this.padding) + parseInt(width * this.mVlaue / 100), this.compHeight / 2);
            ctx.moveTo(parseInt(this.padding) + parseInt(width * this.mVlaue / 100) + textWidth, this.compHeight / 2);
            ctx.lineTo(this.compWidth - this.padding, this.compHeight / 2);
            ctx.stroke();
            ctx.fillText(text, parseInt(this.padding) + parseInt(width * this.mVlaue / 100), this.compHeight / 2);
        }
    },
    setValue(value) {
        this.mVlaue = parseInt(value);
        this.onDraw()
    }
}

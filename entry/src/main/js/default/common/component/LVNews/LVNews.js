/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVNewsController} from '../../component/LVNews/LVNewsController.js'
import {Rect} from '../../component/base/Rect.js'

export default {
    props: {
        controller: {
            default: new LVNewsController()
        }
    },
    data: {
        compWidth: 0,
        compHeight: 0,
        minWidth: 0,
        padding: 5,
        cornerRadius: 9,
        marggingSquareH: 0,
        marggingSquareV: 0,
        marggingLineH: 0,
        marggingLineV: 0,
        rectFSquare: new Rect(),
    },
    onAttached() {
        if (this.controller.getPadding() != -1) {
            this.padding = this.controller.getPadding()
        }
    },
    onPageShow() {
        this.compWidth = parseInt(this.$refs.stack_id.getBoundingClientRect().width)
        this.compHeight = parseInt(this.$refs.stack_id.getBoundingClientRect().height)
        this.minWidth = Math.min(this.compWidth, this.compHeight)
        this.onDraw()
    },
    onDraw() {
        // 获取画布控件
        const ctx = this.$refs.canvas.getContext('2d', {
            antialias: true
        })
        ctx.lineWidth = 3
        ctx.strokeStyle = this.controller.getViewColor();
        ctx.fillStyle = this.controller.getViewColor();
        ctx.globalAlpha = 1;
        ctx.lineCap = 'round';

        // 忽略上一次的绘制结果，只显示下面绘制的内容
        ctx.globalCompositeOperation = 'copy';
        ctx.beginPath()
        // 绘制大边框
        this.drawBigSquare(ctx);
        // 绘制内部的小方框的边
        this.drawContentSquare(ctx);
        // 绘制内部的六条线
        this.drawSixLine(ctx);
        ctx.stroke()
        // 切换回默认显示模式，否则上面绘制的图像将不显示
        ctx.globalCompositeOperation = 'source-over';

        // 绘制小方框里的填充色
        ctx.globalAlpha = 0.3;
        ctx.fillRect(this.rectFSquare.left, this.rectFSquare.top, this.rectFSquare.getWidth(), this.rectFSquare.getHeight());
    },
    drawBigSquare(ctx) {
        ctx.arc(this.padding + this.cornerRadius, this.padding + this.cornerRadius, this.cornerRadius, -Math.PI, -Math.PI / 2)
        ctx.lineTo(this.compWidth - this.padding - this.cornerRadius, this.padding)
        ctx.arc(this.compWidth - this.padding - this.cornerRadius, this.padding + this.cornerRadius, this.cornerRadius, -Math.PI / 2, 0)
        ctx.lineTo(this.compWidth - this.padding, this.compHeight - this.padding - this.cornerRadius)
        ctx.arc(this.compWidth - this.padding - this.cornerRadius, this.compHeight - this.padding - this.cornerRadius, this.cornerRadius, 0, Math.PI / 2)
        ctx.lineTo(this.padding + this.cornerRadius, this.compHeight - this.padding)
        ctx.arc(this.padding + this.cornerRadius, this.compHeight - this.padding - this.cornerRadius, this.cornerRadius, Math.PI / 2, Math.PI)
        ctx.lineTo(this.padding, this.padding + this.cornerRadius)
    },
    drawContentSquare(ctx) {
        if (this.controller.getStep() == 1) {
            this.marggingSquareH = 0 + (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingSquareV = 0;
            this.marggingLineH = 0 + (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingLineV = 0;
        } else if (this.controller.getStep() == 2) {
            this.marggingSquareH = this.compWidth / 2 - this.cornerRadius;
            this.marggingSquareV = 0 + (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingLineH = this.compWidth / 2 - this.cornerRadius;
            this.marggingLineV = 0 + (-this.compWidth / 2 + this.cornerRadius) * this.controller.getAnimValue();
        } else if (this.controller.getStep() == 3) {
            this.marggingSquareH = (this.compWidth / 2 - this.cornerRadius) - (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingSquareV = this.compWidth / 2 - this.cornerRadius;
            this.marggingLineH = (this.compWidth / 2 - this.cornerRadius) - (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingLineV = -this.compWidth / 2 + this.cornerRadius;
        } else if (this.controller.getStep() == 4) {
            this.marggingSquareH = 0;
            this.marggingSquareV = (this.compWidth / 2 - this.cornerRadius) - (this.compWidth / 2 - this.cornerRadius) * this.controller.getAnimValue();
            this.marggingLineH = 0;
            this.marggingLineV = (-this.compWidth / 2 + this.cornerRadius) - (-this.compWidth / 2 + this.cornerRadius) * this.controller.getAnimValue();
        }
        this.rectFSquare.top = this.padding + this.cornerRadius + this.marggingSquareV;
        this.rectFSquare.left = this.padding + this.cornerRadius + this.marggingSquareH;
        this.rectFSquare.bottom = this.compWidth / 2 - this.padding + this.marggingSquareV;
        this.rectFSquare.right = this.compWidth / 2 - this.padding + this.marggingSquareH;
        ctx.moveTo(this.rectFSquare.left, this.rectFSquare.top)
        ctx.lineTo(this.rectFSquare.right, this.rectFSquare.top)
        ctx.lineTo(this.rectFSquare.right, this.rectFSquare.bottom)
        ctx.lineTo(this.rectFSquare.left, this.rectFSquare.bottom)
        ctx.lineTo(this.rectFSquare.left, this.rectFSquare.top)
    },
    drawSixLine(ctx) {
        var line_width_short = (this.compWidth - this.padding - this.cornerRadius - this.marggingLineH) -
        (this.compWidth / 2 + this.padding + this.cornerRadius / 2 - this.marggingLineH);
        var line_width_long = (this.compWidth - this.padding - this.cornerRadius) - (this.padding + this.cornerRadius);
        this.drawShortLine(ctx, line_width_short, 16, 1);
        this.drawShortLine(ctx, line_width_short, 16, 2);
        this.drawShortLine(ctx, line_width_short, 16, 3);
        this.drawlongLine(ctx, line_width_long, 16, 4);
        this.drawlongLine(ctx, line_width_long, 16, 5);
        this.drawlongLine(ctx, line_width_long, 16, 6);
    },
    drawShortLine(ctx, line_width_short, value, positon) {
        positon = positon - 1;
        ctx.moveTo(this.compWidth / 2 + this.padding + this.cornerRadius / 2 - this.marggingLineH,
            this.padding + this.cornerRadius + this.cornerRadius - this.marggingLineV + this.rectFSquare.getHeight() / 3 * positon);
        ctx.lineTo(this.compWidth / 2 + this.padding + this.cornerRadius / 2 - this.marggingLineH + line_width_short / 16.0 * value,
            this.padding + this.cornerRadius + this.cornerRadius - this.marggingLineV + this.rectFSquare.getHeight() / 3 * positon);
    },
    drawlongLine(ctx, line_width_long, value, positon) {
        positon = positon - 4;
        ctx.moveTo(this.padding + this.cornerRadius,
            this.padding + this.cornerRadius + this.rectFSquare.getHeight() / 3 * positon + this.compWidth / 2 + this.marggingLineV);
        ctx.lineTo(this.padding + this.cornerRadius + line_width_long / 16.0 * (value),
            this.padding + this.cornerRadius + this.rectFSquare.getHeight() / 3 * positon + this.compWidth / 2 + this.marggingLineV);
    },
    startAnim() {
        this.controller.startViewAnim(500, this.onDraw)
    },
    startAnimWithTime(time) {
        this.controller.startViewAnim(time, this.onDraw)
    },
    stopAnim() {
        this.controller.stopViewAnim(this.onDraw)
    },
}

/**
 * Copyright (c) 2021 ZhangXiaoqiu
 * LoadingViewJs is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import {LVBase} from '../base/LVBase.js'

export class LVNewsController extends LVBase {
    animValue = 0;
    viewColor = '#FFFFFF';
    mStep = 1;

    getAnimValue() {
        return this.animValue;
    }

    getStep() {
        return this.mStep;
    }

    getViewColor() {
        return this.viewColor;
    }

    setViewColor(viewColor) {
        this.viewColor = viewColor;
        return this;
    }

    OnAnimationUpdate(value) {
        this.animValue = value;
        if (this.animValue > 0 && this.animValue <= 0.25) {
            this.mStep = 1;
        } else if (this.animValue > 0.25 && this.animValue <= 0.5) {
            this.mStep = 2;
            this.animValue = this.animValue - 0.25;
        } else if (this.animValue > 0.5 && this.animValue <= 0.75) {
            this.mStep = 3;
            this.animValue = this.animValue - 0.5;
        } else if (this.animValue > 0.75 && this.animValue <= 1.0) {
            this.mStep = 4;
            this.animValue = this.animValue - 0.75;
        }
        this.animValue = this.animValue / 0.25
    }
}
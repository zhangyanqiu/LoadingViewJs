# LoadingViewJs
&emsp;&emsp;最近在gitee上看到个效果很不错的Loading组件(OpenHarmony-TPC/LoadingView)，想着在鸿蒙JS上将其实现一下，此LoadingViewJs组件便因此诞生了！

## 效果图
<img src="screen/LoadingViewJs.gif"/>

## 如何使用
所有LoadingView的初始化、开始动画、结束动画都是一样的，此处仅以LVCircularCD为例

1.在你的css中定义你的组件宽高（也可以在hml中通过style来定义）
```css
.loading_view {
    width: 150px;
    height: 150px;
}
```
2.在你的hml中写布局，使用你想用的自定义LoadingView组件
```hml
<element name='lv_circular_cd' src='../../common/component/LVCircularCD/LVCircularCD.hml'></element>
<lv_circular_cd 
    class="loading_view" 
    id="lvCircularCD" 
    onclick="start"
    controller="{{ lvCircularCDController }}">
</lv_circular_cd>
```
3.在你的js中通过LoadingViewController初始化LoadingView的属性
```javascript
import {LVCircularCDController} from '../../common/component/LVCircularCD/LVCircularCDController.js'
export default {
    data: {
        lvCircularCDController: null
    },
    onInit() {
        this.lvCircularCDController = new LVCircularCDController()
        .setViewColor("#00FF00") // 设置LoadingView的主色
    },
    start() {
        this.$child('lvCircularCD').stopAnim() // 停止动画
        this.$child('lvCircularCD').startAnim() // 开始执行动画
    },
    stop() {
        this.$child('lvCircularCD').stopAnim() // 停止动画
    }
}
```

除```LVLineWithText```外，所有的LoadingView都有三个方法用来开始和停止动画```startAnim()```、```startAnimWithTime(time)```、```stopAnim()```

```LVLineWithText```则通过```setValue(value)```方法来修改进度值

下表为各LoadingView设置属性的方法

|Id| Picture | Name | Method | 
|---|---|---|---|
1|![1](screen/LVCircularCD.png) | LVCircularCDController | setViewColor(color) // 设置主色
2|![2](screen/LVCircularRing.png) | LVCircularRingController | setViewColor(color)<br>setBarColor(color) // 设置圆弧颜色
3|![3](screen/LVCircular.png) | LVCircularController | setViewColor(color)<br>setRoundColor(color) // 设置外圈小圆颜色
4|![4](screen/LVFivePoiStar.png) | LVFivePoiStarController | setViewColor(color)<br>setCircleColor(color) // 设置圆环颜色
5|![5](screen/LVSmile.png) | LVSmileController | setViewColor(color)
6|![6](screen/LVGears.png) | LVGearsController | setViewColor(color)
7|![7](screen/LVGearsTwo.png) | LVGearsTwoController | setViewColor(color)
8|![8](screen/LVWifi.png) | LVWifiController | setViewColor(color)
9|![9](screen/LVCircularJump.png) | LVCircularJumpController | setViewColor(color)
10|![10](screen/LVCircularZoom.png) | LVCircularZoomController | setViewColor(color)
11|![11](screen/LVPlayBall.png) | LVPlayBallController | setViewColor(color)<br>setBallColor(color) // 设置球的颜色
12|![12](screen/LVNews.png) | LVNewsController | setViewColor(color)
13|![13](screen/LVLineWithText.png) | LVLineWithTextController | setViewColor(color)<br>setTextColor(color) // 设置文字颜色<br>setLineWidth(lineWidth) // 设置线的颜色<br>setTextSize(textSize) // 设置文字大小
14|![14](screen/LVEatBeans.png) | LVEatBeansController | setViewColor(color)<br>setEyeColor(color) // 设置眼睛颜色
15|![15](screen/LVChromeLogo.png) | LVChromeLogoController | 无
16|![16](screen/LVImg.png) | LVImgController | setImgPath(imgPath) // 设置旋转的图片路径
17|![17](screen/LVBlock.png) | LVBlockController | setViewColor(color)<br>setShadowColor(color) // 设置阴影颜色<br>isShadow(show) // 是否显示阴影
18|![18](screen/LVFunnyBar.png) | LVFunnyBarController | setViewColor(color)
19|![19](screen/LVGhost.png) | LVGhostController | setViewColor(color)<br>setEyesColor(color) // 设置眼睛颜色
20|![21](screen/LVBattery.png) | LVBatteryController | setViewColor(color)<br>setCellColor(color) // 设置已充电区域的颜色<br>setTextSize(textSize) // 设置文字大小<br>setPadding(paddingVertical, paddingHorizontal) //设置内间距

## 实现思路
&emsp;&emsp;通过数值动画不停的修改进度值，再让canvas画布组件根据当前进度值重新绘制图形，以此便可达到动画的效果。

## entry运行要求
SDK 6+

## LICENSE
```
Copyright (c) 2021 ZhangXiaoqiu
LoadingViewJs is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
```


